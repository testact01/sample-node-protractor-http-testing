Node.js Sample with Protractor
=================

Implements a Node.js sample with Protractor tests.

Uses Jasmine-Reporters and Istanbul for unit test and code coverage reporting respectively.

#[![Build Status](https://apibeta.shippable.com/projects/540adb63777fb61500da0b1a/badge?branchName=master)](https://beta.shippable.com/projects/540adb63777fb61500da0b1a/builds/latest)
[![Build Status](https://api.shippable.com/projects/54462b3fb904a4b21567c4fe/badge?branchName=master)](https://app.shippable.com/projects/54462b3fb904a4b21567c4fe/builds/latest)